package com.devcamp.jbrs10.bookauthorapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {
    @CrossOrigin
    @GetMapping("/books")
    public ArrayList<Book> getListBook() {
        Author aut1 = new Author("Ngạn", "ngan@mail", 'm');
        Author aut2 = new Author("Tri", "tri@mail", 'm');
        Author aut3 = new Author("Huyen", "huyen@mail", 'f');
        System.out.println(aut1);
        System.out.println(aut2);
        System.out.println(aut3);

        Book bk1 = new Book("Truyện Ma", aut1, 150000);
        Book bk2 = new Book("Truyện Cười", aut2, 90000, 16);
        Book bk3 = new Book("Doremon", aut3, 30000, 100);
        System.out.println(bk1);
        System.out.println(bk2);
        System.out.println(bk3);

        ArrayList<Book> listBook = new ArrayList<>();
        listBook.add(bk1);
        listBook.add(bk2);
        listBook.add(bk3);

        return listBook;
    }
}
